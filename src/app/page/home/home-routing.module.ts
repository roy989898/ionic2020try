import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePage} from './home.page';
import {HeroesComponent} from '../../component/heroes/heroes.component';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule} from '@angular/forms';
import {HeroDetailComponent} from '../../component/hero-detail/hero-detail.component';

const routes: Routes = [
    {
        path: '',
        component: HomePage,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes), CommonModule, IonicModule, FormsModule],
    declarations: [
        HeroesComponent,
        HeroDetailComponent
    ],
    exports: [RouterModule, HeroesComponent]
})
export class HomePageRoutingModule {
}
