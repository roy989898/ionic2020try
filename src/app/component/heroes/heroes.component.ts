import {Component, OnInit} from '@angular/core';
import {Hero} from '../../hero';
import {HEROES} from '../../mock-heroes';
import {HeroService} from '../../hero.service';
import {Observable} from 'rxjs';
import {MessageService} from '../../message.service';

@Component({
    selector: 'app-heroes',
    templateUrl: './heroes.component.html',
    styleUrls: ['./heroes.component.scss'],
})
export class HeroesComponent implements OnInit {
    hero: Hero = {
        id: 1,
        name: 'Windstorm'
    };

    selectedHero: Hero;

    heroes$: Observable<Hero[]>;


    constructor(private heroservice: HeroService, private messageService: MessageService) {
    }

    ngOnInit() {
        this.heroes$ = this.heroservice.getHeroes();
    }


    onSelect(hero: Hero): void {
        this.selectedHero = hero;
        this.messageService.add(`HeroesComponent: Selected hero id=${hero.id}`);
    }

}
